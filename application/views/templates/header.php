<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>CI Test 1</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width">

    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
    <link rel="shortcut icon" href="favicon.ico">

    <link rel="stylesheet" href="<?php echo base_url(); ?>public/css/normalize.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>public/css/main.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>public/css/responsive.css">
    
    <script src="<?php echo base_url(); ?>public/js/vendor/modernizr-2.6.2.min.js"></script>
    <!-- Grab Google CDN's jQuery. fall back to local if necessary -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script>window.jQuery || document.write("<script src='<?php echo base_url('public');?>/js/vendor/jquery-1.9.1.min.js'>\x3C/script>")</script>
    <script src="<?php echo base_url('public');?>/js/responsive-nav.js"></script>
    <script src="<?php echo base_url('public');?>/js/tinynav.min.js"></script>
	<script>
    $(function () {
    
		// TinyNav.js 1
		$('#top-nav').tinyNav({
			//active: 'selected',
			header: 'User Menu',
		});
		
		// TinyNav.js 2
		$('#bot-nav').tinyNav({
			//header: 'Bot Nav',
			//active: 'selected'
		});
    
    });
    </script>
    
    <!-- Application-specific meta tags -->
	<!-- Windows 8 -->
	<meta name="application-name" content="" /> 
	<meta name="msapplication-TileColor" content="" /> 
	<meta name="msapplication-TileImage" content="" />
	<!-- Twitter -->
	<meta name="twitter:card" content="">
	<meta name="twitter:site" content="">
	<meta name="twitter:title" content="">
	<meta name="twitter:description" content="">
	<meta name="twitter:url" content="">
	<!-- Facebook -->
	<meta property="og:title" content="" />
	<meta property="og:description" content="" />
	<meta property="og:url" content="" />
	<meta property="og:image" content="" />
</head>
<body id="top">
    <?php
	if($this->ion_auth->logged_in()) {
		$user = $this->ion_auth->user()->row();
	}
	else {
		$user = new stdClass();
		$user->first_name = 'Guest';
		$user->last_name = 'User';
	}
	/*
	stdClass Object (
		[id] => 1
		[ip_address] => 127.0.0.1
		[username] => administrator
		[password] => 59beecdf7fc966e2f17fd8f65a4a9aeb09d4a3d4
		[salt] => 9462e8eee0
		[email] => admin@admin.com
		[activation_code] => 19e181f2ccc2a7ea58a2c0aa2b69f4355e636ef4
		[forgotten_password_code] => 81dce1d0bc2c10fbdec7a87f1ff299ed7e4c9e4a
		[remember_code] => 9d029802e28cd9c768e8e62277c0df49ec65c48c
		[created_on] => 1268889823
		[last_login] => 1279464628
		[active] => 0
		[first_name] => Admin
		[last_name] => Account
		[company] => Some Corporation
		[phone] => (123)456-7890
	)
	*/
	//echo $user->email;
	?>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->
        <!-- Header -->
        <header id="header">
        	<div class="container-12">
            	<div class="group">
                    <div id="logo" class="grid-3">
                        <a href="javascript:void(0);"><img id="logo-img" src="<?php echo base_url(); ?>public/img/logo.png" title="CI Test 1" alt="CI Test 1" /></a>
                    </div>
                    
                        <nav class="grid-9">
                            <ul id="top-nav" class="nav-top">
                                <?php if($this->ion_auth->logged_in()) { ?>
                                <li><a href="<?php echo site_url('auth/profile'); ?>">Profile<?php //echo $user->first_name; ?> <?php //echo $user->last_name; ?></a></li>
                                <li><a href="<?php echo site_url('auth/logout'); ?>">Logout</a></li>
                                <?php } else { ?>
                                <li><a href="<?php echo site_url('auth/login'); ?>">Login</a></li>
                                <li><a href="<?php echo site_url('auth/register'); ?>">Register</a></li>
                                <?php } ?>
                            </ul>
                        </nav>
                    
                        <nav class="grid-9">
                            <ul id="bot-nav" class="nav-bot">
                                <li<?php echo ($page == 'home' ? ' class="selected"' : '') ?>><a href="<?php echo site_url('home'); ?>">Home</a></li>
                                <li<?php echo ($page == 'about' ? ' class="selected"' : '') ?>><a href="<?php echo site_url('about'); ?>">About</a></li>
                                <li><a href="javascript:void(0);">MenuItem1</a></li>
                                <li><a href="javascript:void(0);">MenuItem2</a></li>
                                <li><a href="javascript:void(0);">MenuItem3</a></li>
                                <li><a href="javascript:void(0);">MenuItem4</a></li>
                            </ul>
                        </nav>
                    
                </div>
                
            </div>
        </header>
        <!-- Header END -->
    	
        <!-- Content -->
        <div id="content" role="main" style="margin-top: 100px;">
            <section class="container-12">
                
                <section id="breadcrumbs" class="group">
                    <div class="grid-12">
                        <ul class="description">
                            <li><span class="attribute">Breadcrumbs:</span></li>
                            <li><span class="attribute"><?php echo $title; ?></span></li>
                        </ul>
                    </div>                        
                </section>
                
                <!-- Title -->
                <section id="page-title" class="group">
                    <header class="grid-12 title">
                        <h3><?php echo $title; ?></h3>
                    </header>
                </section>
                <!-- Title END  -->