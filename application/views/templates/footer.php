
        	</section>
        </div>
        <!-- Content END -->
        
        <!-- Footer -->
        <div class="container-12">
            <div class="group">
                <footer id="footer" class="grid-12">
                    <p>Made by <a href="javascript:void(0);">Git-R-Done</a></p>
                </footer>   
            </div>
            
            <p id="back-top">
            	<a href="#top"><span></span>Back to Top</a>
            </p>
             
        </div>  
        <!-- Footer END -->
        <script src="<?php echo base_url(); ?>public/js/plugins.js"></script>
        <script src="<?php echo base_url(); ?>public/js/functions.js"></script>
    </body>
</html>