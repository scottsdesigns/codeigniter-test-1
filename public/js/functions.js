// remap jQuery to $
(function($) {


/* trigger when page is ready */
$(document).ready(function () {
	// hide #back-top first
	$("#back-top").hide();
	
	// fade in #back-top
	$(function () {
		$(window).scroll(function () {
			if ($(this).scrollTop() > 100) {
				$('#back-top').fadeIn();
			} else {
				$('#back-top').fadeOut();
			}
		});

		// scroll body to 0px on click
		$('#back-top a').click(function () {
			$('body,html').animate({
				scrollTop: 0
			}, 800);
			return false;
		});
	});
});

// your functions go here
window.onscroll=function () {
	var top = window.pageXOffset ? window.pageXOffset : document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop;
	if(top > 50) {
		//document.getElementById("header").style.position = "fixed";
		$("#header").css("position", "fixed");
		document.getElementById("header").style.height = "50px";
		document.getElementById("logo-img").style.height = "50px";
		$("#top-nav").addClass("hidden");
		$("select#tinynav1").css("top", "1px");
		$("select#tinynav2").css("top", "26px");
	}
	else {
		//document.getElementById("header").style.position = "absolute";
		$("#header").css("position", "absolute");
		document.getElementById("header").style.height = "100px";
		document.getElementById("logo-img").style.height = "100px";
		$("#top-nav").removeClass("hidden");
		$("select#tinynav1").css("top", "18px");
		$("select#tinynav2").css("top", "58px");
	}
}


})(window.jQuery);



/*
header#header nav select#tinynav1 {
	right: 0px;
	top: 18px;
}
header#header nav select#tinynav2 {
	right: 0px;
	top: 58px;
}
*/